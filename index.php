<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/x-icon" href="favicon.ico"/>
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Krub" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="main.js"></script>
    <title>Weather-App</title>
</head>

<body>

<div id="wrapper">

    <video muted loop id="backgroundVideo">
        <source id="backgroundVideoSrc" src="" type="video/mp4">
    </video>

    <audio loop id="backgroundAudio">
        <source id="backgroundAudioSrc" src="" type="audio/mpeg">
    </audio>
    
    <div id="weatherContainer">
        <h1>How's the weather in:</h1>
        <input type="text" id="city" placeholder="City" autofocus>
    </div>

    <div id="weatherInfo"></div>

</div>

</body>
</html>