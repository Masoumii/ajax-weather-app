$(function(){
        
        /* App Settings */
        const apiKey = "3b7f9acf0824add30b878e92abd84f4c"; // OpenWeatherMap API Key
        const units = "metric"; // metric(Celcius) or imperial(Fahrenheit)

        /* Create loading icon element */
        const loadingIcon = $("<img src='resources/images/loading.svg' width='100'>"); 

        /* Change cursor pointer on focus out */
        $("input[type='text']").focusout(function(){
                $(this).css({"cursor":"default"});
        });

        /* Change cursor pointer on input click */
        $("input[type='text']").on("click", function(){
                $(this).css({"cursor":"text"});
        });

        /* On keypress */
        $(document).keypress(function(e) {

                /* if "Enter" Was pressed */
                if(e.which == 13) {

                        /* Show loading animation while loading data */
                        $("#weatherInfo").html(loadingIcon);
                        
                        /* Enable video overlay */
                        $("#backgroundVideo").css({"visibility":"visible"});

                        /* Remove input focus */
                        $("input[type='text']").blur();

                        /* Hide Title */
                        $("h1").hide();

                        /* Get user input value */
                        let cityToQuery = $("#city").val();

                        /* Get weather data based on user-input from the API, extract the data and assign to variables */
                        $.get("https://api.openweathermap.org/data/2.5/weather?q="+cityToQuery+"&units="+units+"&appid="+apiKey, function(data){  

                                /* Initial Background Video & Audio source variables */
                                var audio = $("body").find('audio')[0];    
                                audio.pause(); // Stop playing
                                audio.currentTime = 0; // Reset time

                                /* Weather Object */
                                let weatherMain        = data.weather[0].main;
                                let weatherDescription = data.weather[0].description;
                                let weatherIcon        = data.weather[0].icon;

                                /* Main Object */
                                let mainTemp           = data.main.temp;
                                let mainPressure       = data.main.pressure;
                                let mainHumidity       = data.main.humidity;
                                let mainTempMin        = data.main.temp_min;
                                let mainTempMax        = data.main.temp_max;

                                /* Visibility Object */
                                let Visibility         = data.visibility;

                                /* Wind Object */
                                let windSpeed          = data.wind.speed;
                                let windDegree         = data.wind.deg;

                                /* Cloud Object */
                                let Cloud              = data.clouds.all;

                                /* System Object */
                                let systemSunriseHour = new Date(data.sys.sunrise*1000).getHours();
                                let systemSunriseMin  = new Date(data.sys.sunrise*1000).getMinutes();
                                let systemSunsetHour  = new Date(data.sys.sunset*1000).getHours();
                                let systemSunsetMin   = new Date(data.sys.sunset*1000).getMinutes();

                                /* Function to format sunrise minutes */
                                function addLeadingZeroSunrise(minute){
                                        if(minute<10){
                                                systemSunriseMin = "0"+minute;
                                         }else{
                                                systemSunriseMin = minute;
                                         }
                                        return systemSunriseMin;
                                }

                                /* Function to format sunset minutes */
                                function addLeadingZeroSunset(minute){
                                        if(minute<10){
                                                systemSunsetMin = "0"+minute;
                                        }else{
                                               systemSunsetMin = minute;
                                        }
                                        return systemSunsetMin;
                                }

                                /* Format sunrise and sunset minutes and return values */
                                addLeadingZeroSunrise(systemSunriseMin);
                                addLeadingZeroSunset(systemSunsetMin);

                                /* Combine hours and minutes and assign to variable */
                                let systemSunrise     = systemSunriseHour+":"+systemSunriseMin;
                                let systemSunset      = systemSunsetHour+":"+systemSunsetMin;

                                /* Background Audio-file Sources */
                                let audioRaining        = "raining.mp3";
                                let audioLightRaining   = "light-raining.mp3";
                                let audioSunny          = "sunny.mp3";
                                let audioStorm          = "stormy.mp3";
                                let audioSnow           = "snowing.mp3";
                                let audioWindy          = "windy.mp3";
                                let audioLightWindy     = "light-windy.mp3";

                                /* Background Video-file Sources */
                                let videoRaining        = "raining.mp4";
                                let videoSunny          = "sunny.mp4";
                                let videoStorm          = "storm.mp4";
                                let videoSnow           = "snowing.mp4";
                                let videoFoggy          = "foggy.mp4";
                                let videoCloudy         = "cloudy.mp4";
                                let videoLightCloudy    = "light-cloudy.mp4"
                                let videoSand           = "sandstorm.mp4";

                                /* Determine Background & Video source variables depending on the current weather Conditions */
                                switch (weatherMain) {

                                        case "Clear":
                                                backgroundVideoSrc = videoSunny;
                                                backgroundAudioSrc = audioSunny;
                                        break;

                                        case "Rain":
                                                backgroundVideoSrc = videoRaining;
                                                backgroundAudioSrc = audioLightRaining;
                                        break;

                                        case "Drizzle":
                                                backgroundVideoSrc = videoRaining;
                                                backgroundAudioSrc = audioLightRaining;
                                        break;

                                        case "Storm":
                                                backgroundVideoSrc = videoStorm;
                                                backgroundAudioSrc = audioStorm;
                                        break;

                                        case "Snow":
                                                backgroundVideoSrc = videoSnow;
                                                backgroundAudioSrc = audioSnow;
                                        break;

                                        case "Smoke":
                                                backgroundVideoSrc = videoFoggy;
                                                backgroundAudioSrc = audioWindy;
                                        break;

                                        case "Haze":
                                                backgroundVideoSrc = videoFoggy;
                                                backgroundAudioSrc = audioWindy;
                                        break;

                                        case "Mist":
                                                backgroundVideoSrc = videoFoggy;
                                                backgroundAudioSrc = audioWindy;
                                        break;

                                        case "Fog":
                                                backgroundVideoSrc = videoFoggy;
                                                backgroundAudioSrc = audioWindy;
                                        break;

                                        case "Clouds":
                                                backgroundVideoSrc = videoLightCloudy;
                                                backgroundAudioSrc = audioLightWindy;
                                        break;

                                        case "Sand":
                                                backgroundVideoSrc = videoSand;
                                                backgroundAudioSrc = audioWindy;
                                        break;

                                        default:
                                                alert("Undefined weather description.");
                                }

                                        /* Initialise the Weather App */

                                        // Video
                                        videoToPlay = $("#backgroundVideoSrc").attr("src", "resources/videos/"+backgroundVideoSrc);
                                        let backgroundVideo = $("#backgroundVideo");
                                        backgroundVideo.append(videoToPlay);
                                        backgroundVideo[0].load();
                                        backgroundVideo[0].play();

                                        // Audio
                                        audioToPlay = $("#backgroundAudioSrc").attr("src", "resources/audio/"+backgroundAudioSrc);
                                        let backgroundAudio = $("#backgroundAudio");
                                        backgroundAudio.append(audioToPlay);
                                        backgroundAudio[0].load();
                                        backgroundAudio[0].play();
                                                // Loop Audio if ended
                                                backgroundAudio[0].addEventListener('ended', function() {
                                                        this.currentTime = 0;
                                                        this.play();
                                                }, false);

                                        // Information
                                        $("#weatherInfo").html(
                                                "<b>"+"<img src='http://openweathermap.org/img/w/"+weatherIcon+".png' width='80'><br>"+weatherDescription.toUpperCase()+"</b>"+
                                                "<br><br>"+
                                                "<img src='resources/images/temp.svg' width='35' title='Temperature'>&nbsp;"+mainTemp+"℃"+"<br><br>"+
                                                "<img src='resources/images/humidity.svg' width='35' title='Humidity'>&nbsp;"+mainHumidity+"%"+"<br><br>"+
                                                "<img src='resources/images/sunrise.svg' width='35' title='Sunrise'>&nbsp;"+systemSunrise+"<br><br>"+
                                                "<img src='resources/images/sunset.svg' width='35' title='Sunset'>&nbsp;"+systemSunset+"<br><br>"
                                        ).fadeIn();
                                        
                        /* If (response)status 404 or City not found */                 
                        }).fail(function(){ 
                                $("#weatherInfo").hide().html("City not found or service currently unavailable.<br><br>Please use English city names only<br> or try again later!").fadeIn();
                        });
                }
        })
})